$( function() {
  var $picker = $('.input--icon');
  $picker.datepicker({
      dateFormat: "dd-mm-yy",
      buttonImageOnly: true,
      buttonImage: './ui-icons_469bdd_256x240.png',
      autoClose: true,
      clearButton: true,
      beforeShow: function(dp, animationCompleted) {
        document.addEventListener("touchstart", touchStart, false);
        function touchStart(event) {
          var target = $( event.target )
          if ( ! target.parents().hasClass('ui-datepicker')) {
            $picker.datepicker("hide").data('datepicker').hide()
          }
        }
      }
  });
});
