$(document).ready(function(){  
    $('.swiper-wrapper a').not('.swiper-slide-duplicate').magnificPopup({
      type: 'image',
      gallery: {
        enabled:true
      }
    })

    $('.section-tours__gallery a').magnificPopup({
      type: 'image',
      gallery: {
        enabled:true
      }
    })

    $('.button__order,#call,#3d_tour').magnificPopup({
      type: 'ajax',
      closeMarkup: '<div class="popup-close"></div>'
    });

    $('#home-order').on('click',function(event) {
        event.preventDefault();
        var date_start = $('#date_start').val(),
            date_end = $('#date_end').val();
        if( date_start=="" && date_end=="") {
          swal({
            title: "Ошибка",
            text: "Заполните пожалуйста все поля!",
            icon: "error"
          });
        }
        else if(date_start=="") {
          swal({
            title: "Ошибка",
            text: "Введите пожалуйста дату заезда!",
            icon: "error"
          });
        }
        else if(date_end=="") {
          swal({
            title: "Ошибка",
            text: "Введите пожалуйста дату отъезда!",
            icon: "error"
          });
        }
        else {
          $('#home-order').magnificPopup({
            type: 'ajax',
            ajax: {
              settings:{
                data:{
                  date_start: date_start,
                  date_end: date_end
                }
              }
            },
            closeMarkup: '<div class="popup-close"></div>'
          })          
        }
    })

    $('#contact-order').on('click',function (event) {
      event.preventDefault()
      var name = $('#contact-name').val(),
          email = $('#contact-email').val(),
          description = $('#contact-message').val()

      if(name=='' && email=='' && description=='' ) {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста все поля!",
          icon: "error"
        });
      }
      else if(name=='') {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста поле имя!",
          icon: "error"
        });
      }
      else if(email=='') {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста поле email!",
          icon: "error"
        });
      }
      else if(description=='') {
        swal({
          title: "Ошибка",
          text: "Добавтье пожалуйста сообщение!",
          icon: "error"
        });
      }
      else {
        $.ajax({
          url: 'handlers/contact.php',
          type: 'post',
          data: {
            name: name,
            email: email,
            description: description
          }
        })
        .done(function(data) {
          $('#contact-name').val('')
          $('#contact-email').val('')
          $('#contact-message').val('')
          swal({
            title: data,
            icon:"success"
          });
        })
        .fail(function() {
          swal({
            title: "Ошибка!",
            text: "Попробуйте еще раз",
            icon: "error",
          });
        });
      }
      
    })

    $('#reservetion-order').on('click',function (event) {
      event.preventDefault()
      var name = $('#reservetion-name').val(),
          email = $('#reservetion-email').val(),
          phone = $('#reservetion-phone').val(),
          date_start = $('#reservetion-start').val(),
          date_end = $('#reservetion-end').val(),
          room_type = $('#reservetion-type').val(),
          description = $('#reservetion-description').val(),
          guest_number = $('#reservetion-guest').val()

      if( name=='' && email=='' && phone=='' && date_start=='' && date_end=='' && room_type==0) {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста все поля!",
          icon: "error"
        });
      }
      else if(name=='') {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста поле имя!",
          icon: "error"
        });
      }
      else if(phone=='') {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста поле телефон!",
          icon: "error"
        });
      }
      else if(email=='') {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста поле email!",
          icon: "error"
        });
      }
      else if(date_start=='') {
        swal({
          title: "Ошибка",
          text: "Пожалуйста, введите дату заезда!",
          icon: "error"
        });
      }
      else if(date_end=='') {
        swal({
          title: "Ошибка",
          text: "Пожалуйста, введите дату отъезда!",
          icon: "error"
        });
      }
      else if(room_type=='') {
        swal({
          title: "Ошибка",
          text: "Пожалуйста, выберите тип номера!",
          icon: "error"
        });
      }
      else {
        $.ajax({
          url: 'handlers/reservetion.php',
          type: 'post',
          data: {
            name: name,
            email: email,
            phone: phone,
            date_start: date_start,
            date_end: date_end,
            room_type: room_type,
            guest_number: guest_number,
            description: description
          }
        })
        .done(function(data) {
          $('.form__group input').val('')
          $('#reservetion-type').val(0)         
          $('#reservetion-description').val('')
          $('#reservetion-guest').val(0)
         
          swal({
            title: data,
            icon:"success"
          });
        })
        .fail(function() {
          swal({
            title: "Ошибка!",
            text: "Попробуйте еще раз",
            icon: "error",
          });
        });
      }
      
    })

    $('.room_1').magnificPopup({
      type: 'image',
      gallery: {
        enabled:true
      }
    })
    $('.room_2').magnificPopup({
      type: 'image',
      gallery: {
        enabled:true
      }
    })
    $('.room_3').magnificPopup({
      type: 'image',
      gallery: {
        enabled:true
      }
    })
    $('.room_4').magnificPopup({
      type: 'image',
      gallery: {
        enabled:true
      }
    })
})