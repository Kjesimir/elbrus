$( function() {
var swiper = new Swiper('.section-slider .swiper-container', {
    autoplay: {
      delay: 5500
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    loop: true,
    lazy: true
});

var aboutSwiper = new Swiper('.section-about__slider', {
  autoplay:false,
  loop:true,
  lazy: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }
});

});