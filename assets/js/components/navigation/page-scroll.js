$(document).ready(function() {
  // Add smooth scrolling to all links
  $(".navigation__container a, .section-slider__description a, .header__container a").on('click', function(event) {
    $('body').toggleClass( "nav-opened" );
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior


      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area

      var difference_top = -300;

      if (hash=='#section-services'||hash=='#section-rooms') {
         difference_top = -450;
      }

      $('html, body').animate({
        scrollTop: ($(hash).offset().top + difference_top)
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
      $(".navigation__container a").removeClass('active');
      $(this).not('.header__container a,.section-slider__description a').addClass('active');
    } // End if
  });    
});

$(document).ready(function() {  
  const $sections = $('section')
  window.addEventListener('scroll',function(){
    $sections.each(function(index,element) {
      switchingOnScroll(element)
    })

  })


  
  var prevScrollpos = window.pageYOffset;  

  window.addEventListener('scroll',function() {
    var currentScrollPos = window.pageYOffset;
    if(pageYOffset < 88 || currentScrollPos<prevScrollpos) {
      $('.header').css('top','0')
    }
    else {
      if( window.innerWidth > 720 && window.innerWidth <= 900 ) {
        $('.header').css('top','-90px')
      }
      else if(window.innerWidth > 900 && window.innerWidth <= 1024) {
        $('.header').css('top','-74px')
      }
      else if( window.innerWidth > 1024 ) {
        $('.header').css('top','-88px')
      }
    }
    prevScrollpos = currentScrollPos;
  }) 
  
})


function switchingOnScroll(section) {

  var difference_top = 350;
  if (section.id=='section-rooms'||section.id=='section-services') {
    difference_top = 500;
  }

  var top = section.offsetTop - difference_top,
   bottom = top + section.offsetHeight,
   scroll = window.scrollY,
   id = section.id

  if (scroll > top && scroll < bottom) {
    $('.navigation__container a').removeClass('active')
    $(`.navigation__container a[href*='#${id}']`).addClass('active')
  }
}
