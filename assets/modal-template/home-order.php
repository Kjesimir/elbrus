<div class="popup popup-home">
    <h2 class="popup__title">БРОНИРОВАНИЕ НОМЕРА</h2>
    <p class="popup__description">Заполните форму и наш менеджер забронирует для вас номер и свяжется с вами в течение 15 минут</p>
  
    <div class="popup-form">
      <input class="input input--second" id="name" type="text" placeholder="Ваше имя">
      <input class="input input--second" id="email" type="email" placeholder="e-mail">
  
      <input type="hidden" id="date_start" placeholder="Дата заезда" name="arrival" class="input input--second input--icon" value="<?php echo $_GET['date_start']; ?>" >
      <input type="hidden" id="date_end" placeholder="Дата отъезда" name="departure" class="input input--second input--icon" value="<?php echo $_GET['date_end']; ?>">
  
      <input type="button" id="btn-home" value="Забронировать номер" class="button" onclick="yaCounter12755839.reachGoal('home-send'); return true;">
    </div>
  </div>
  <script>
    $('.popup-close').on('click',function(){
      $.magnificPopup.close()
    })

    $('#btn-home').on('click',function(event){
      event.preventDefault();
      var name = $('#name').val(),
        email = $('#email').val(),
        date_start = $('#date_start').val(),
        date_end = $('#date_end').val();
  
      if(name=="" && email=="") {
        swal({
          title: "Ошибка",
          text: "Заполните пожалуйста все поля!",
          icon: "error"
        });
      }
      else if(name=="") {
        swal({
          title: "Ошибка",
          text: "Введите пожалуйста имя!",
          icon: "error"
        });
      }
      else if(email=="") {
        swal({
          title: "Ошибка",
          text: "Введите пожалуйста email!",
          icon: "error"
        });
      }
      else if(date_start=="") {
        swal({
          title: "Ошибка",
          text: "Введите пожалуйста дату заезда!",
          icon: "error"
        });
      }
      else if(date_end=="") {
        swal({
          title: "Ошибка",
          text: "Введите пожалуйста дату отъезда!",
          icon: "error"
        });
      }
      else {
          $.ajax({
              url: "handlers/home-order.php",
              type:'post',
              data: {
                name: name,
                email: email,
                date_start: date_start,
                date_end: date_end 
              }
          })
          .done(function(data) {
              $.magnificPopup.close();
              swal({
                  title: "Спасибо за заявку!",
                  icon: "success"
              });
          })
          .fail(function() {
              swal({
                  title: "Ошибка!",
                  text: "Попробуйте еще раз",
                  icon: "error"
              });
          });
      }    
    })

  </script>