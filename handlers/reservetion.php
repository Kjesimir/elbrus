<?php 


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require_once 'vendor/autoload.php';
require_once 'config.php';
require_once 'helpers.php';

$name = $_POST['name'];
$email = $_POST['email'];
$date_start = $_POST['date_start'];
$date_end= $_POST['date_end'];
$phone = $_POST['phone'];
$room_type = $_POST['room_type'];
$guest_number = $_POST['guest_number'];
$description = $_POST['description'];

check_fields([ $name, $email, $guest_number, $date_start, $date_end, $room_type]);
$message = '<strong>Имя: </strong>' . $name . '<br /><strong>email: </strong>' . $email. '<br /><strong>Телефон: </strong>' . $phone. '<br /><strong>Дата заезда: </strong>' . $date_start. '<br /><strong>Дата отъезда: </strong>' . $date_end. '<br /><strong>Тип номера: </strong>' . $room_type. '<br /><strong>Количество: </strong>' . $guest_number. '<br /><strong>Сообщение: </strong>' . $description;

$mail = new PHPMailer(true);

 try {
    $mail->setFrom(Config::FROM, Config::NAME);
    $mail->addAddress(Config::TO);
    $mail->AddBCC('lead@danifo.ru');
    $mail->Subject = 'Эльбрусия.Забронировать номер.';
    $mail->CharSet = 'UTF-8';
    $mail->isHTML(true); 
    $mail->Body = $message;
    $mail->send();
    http_response_code(200);
    echo 'Собщение успешно отправлено';
} catch(Exception $e) {
  http_response_code(403);
  echo 'Произошла ошибка при отправке. ' . $mail->ErrorInfo;
}
?>