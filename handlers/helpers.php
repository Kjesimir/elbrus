<?php 

function check_fields( $fildes ) {


  foreach ($fildes as $field) {
    if(empty($field)) {
      http_response_code('403');
      echo 'Заполните пожалуйста все поля';
      exit;
    }
  }
}

function check_request() {
  if ($_SERVER['REQUEST_METHOD']!= "POST") {
    http_response_code('500');
    echo "Произошла ошибка на сервере";
    exit;
  }
}
?>