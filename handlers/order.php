<?php 


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require_once 'vendor/autoload.php';
require_once 'config.php';
require_once 'helpers.php';

$name = $_POST['name'];
$email = $_POST['email'];
$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];

check_fields([ $name, $email, $date_start, $date_end ]);
$message = '<strong>Имя: </strong>' . $name . '<br /><strong>email: </strong>' . $email. '<br /><strong>Дата заезда: </strong>' . $date_start. '<br /><strong>Дата отъезда: </strong>' . $date_end;

$mail = new PHPMailer(true);

 try {
    $mail->setFrom(Config::FROM, Config::NAME);
    $mail->addAddress(Config::TO);
    $mail->AddBCC('lead@danifo.ru');
    $mail->Subject = 'Эльбрусия.Забронировать номер.';
    $mail->CharSet = 'UTF-8';
    $mail->isHTML(true); 
    $mail->Body = $message;
    $mail->send();
    http_response_code(200);
    echo 'Заявка успешно отправлена';
} catch(Exception $e) {
  http_response_code(403);
  echo 'Произошла ошибка при отправке. ' . $mail->ErrorInfo;
}
?>