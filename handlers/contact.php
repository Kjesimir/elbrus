<?php 


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require_once 'vendor/autoload.php';
require_once 'config.php';
require_once 'helpers.php';

$name = $_POST['name'];
$email = $_POST['email'];
$description = $_POST['description'];

check_fields([ $name, $email, $description]);
$message = '<strong>Имя: </strong>' . $name . '<br /><strong>email: </strong>' . $email. '<br /><strong>Сообщение: </strong>' . $description;

$mail = new PHPMailer(true);

 try {
    $mail->setFrom(Config::FROM, Config::NAME);
    $mail->addAddress(Config::TO);
    $mail->AddBCC('lead@danifo.ru');
    $mail->Subject = 'Эльбрусия.Забронировать номер.';
    $mail->CharSet = 'UTF-8';
    $mail->isHTML(true); 
    $mail->Body = $message;
    $mail->send();
    http_response_code(200);
    echo 'Собщение успешно отправлено';
} catch(Exception $e) {
  http_response_code(403);
  echo 'Произошла ошибка при отправке. ' . $mail->ErrorInfo;
}
?>